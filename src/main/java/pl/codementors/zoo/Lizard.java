package pl.codementors.zoo;

public abstract  class Lizard extends Animal{

    private String scaleColor;

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }

    public Lizard(String name, int age, String scaleColor) {
        super(name, age);
        this.scaleColor = scaleColor;
    }

    public Lizard() {
    }

    public Lizard(String name) {
        super(name);
    }
}
