package pl.codementors.zoo;

public class Iguana extends Lizard implements Herbivorous{

    public void hiss(){
        System.out.println(getName() + " is hissing");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " is eating");

    }

    public Iguana(String name, int age, String scaleColor) {
        super(name, age, scaleColor);
    }

    public Iguana() {
    }

    @Override
    public void eatPlant() {
        System.out.println(getName() + " is eating plant");

    }

    public Iguana(String name) {
        super(name);
    }
}
