package pl.codementors.zoo;

public class Parrot extends Bird implements Herbivorous{

    public void screech(){
        System.out.println(getName() + " is screeching");
    }

    @Override
    void eat() {
        System.out.println(getName() + " is eating");
    }

    public Parrot(String name, int age, String featherColor) {
        super(name, age, featherColor);
    }

    public Parrot(String name) {
        super(name);
    }

    @Override
    public void eatPlant() {
        System.out.println(getName() + " is eating plant");
    }

    public Parrot() {
    }
}
