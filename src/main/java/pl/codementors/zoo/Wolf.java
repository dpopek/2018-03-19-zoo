package pl.codementors.zoo;

public class Wolf extends Mammal implements Carnivorous{

    public void howl(){
        System.out.println(getName() + " is howling");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " is eating");
    }

    public Wolf(String name, int age, String furColor) {
        super(name, age, furColor);
    }

    public Wolf() {
    }

    public void printTypeName(){
        System.out.println("Wolf " + getName() );
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + " is eating meat");
    }

    public Wolf(String name) {
        super(name);
    }
}
