package pl.codementors.zoo;

public abstract class Mammal extends Animal{

    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    public Mammal(String name, int age, String furColor) {
        super(name, age);
        this.furColor = furColor;
    }

    public Mammal() {
    }

    public Mammal(String name) {
        super(name);
    }
}
