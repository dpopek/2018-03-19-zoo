package pl.codementors.zoo;

import java.io.*;


public class ZooMain {
    public static void main(String[] args) throws IOException {

        System.out.println("Hello to the ZOO");

        Animal[] animalsArray = new Animal[10];
        animalsArray[0] = new Parrot("Parrot1", 3, "yellow");
        animalsArray[1] = new Iguana("Iguana1", 6, "green");
        animalsArray[2] = new Iguana("Iguana2", 1, "grey");
        animalsArray[3] = new Wolf("Wolf1", 5, "black");
        animalsArray[4] = new Parrot("Parrot2", 2, "red");
        animalsArray[5] = new Parrot("Parrot3", 1, "green");
        animalsArray[6] = new Wolf("Wolf2", 7, "grey");
        animalsArray[7] = new Wolf("Wolf3", 9, "pink");
        animalsArray[8] = new Iguana("Iguana3", 10, "yellow");
        animalsArray[9] = new Parrot("Parrot4", 5, "blue");

        readFromBinaryFile("/tmp/binary");
        
    }


    static void print(Animal Animal[]) {
        for (Animal animal : Animal) {
            if (animal instanceof Parrot) {
                System.out.println("Type Parrot, name " + animal.getName());
            } else if (animal instanceof Iguana) {
                System.out.println("Type Iguana, name " + animal.getName());
            } else if (animal instanceof Wolf) {
                System.out.println("Type Wolf, name " + animal.getName());
            }
        }
    }

    static void feed(Animal Animal[]) {
        for (Animal animal : Animal) {
            animal.eat();
        }
    }

    static void saveToFile(Animal Animal[], String filePath) {
        FileWriter fw = null;
        String line = null;
        try {
            fw = new FileWriter(filePath);
            for (Animal animal : Animal) {
                if (animal instanceof Parrot) {
                    line = ("Type Parrot, name " + animal.getName());
                } else if (animal instanceof Iguana) {
                    line = ("Type Iguana, name " + animal.getName());
                } else if (animal instanceof Wolf) {
                    line = ("Type Wolf, name " + animal.getName());
                }
                fw.write(line + '\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static void howl(Animal Animal[]) {
        for (Animal animal : Animal) {
            if (animal instanceof Wolf) {
                ((Wolf) animal).howl();
            }
        }

    }

    static void hiss(Animal Animal[]) {
        for (Animal animal : Animal) {
            if (animal instanceof Iguana) {
                ((Iguana) animal).hiss();
            }
        }

    }

    static void screech(Animal Animal[]) {
        for (Animal animal : Animal) {
            if (animal instanceof Parrot) {
                ((Parrot) animal).screech();
            }
        }

    }

    static void feedWithMeat(Animal Animal[]) {
        for (Animal animal : Animal) {
            if (animal instanceof Carnivorous){
                ((Carnivorous) animal).eatMeat();
            }
        }
    }

    static void feedWithPlant(Animal Animal[]) {
        for (Animal animal : Animal) {
            if (animal instanceof Herbivorous){
                ((Herbivorous) animal).eatPlant();
            }
        }
    }

    static void readFromFile(String filePath){
        Animal[] animalsArray2 = new Animal[10];
        String line = null;


        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            for (Animal animal:animalsArray2) {
                while (br.ready()) {
                    line = br.readLine();
                    String[] nextAnimal = line.split("\\s");
                    String type = nextAnimal[1];
                    String name = nextAnimal[3];
                    if(type.equals("Wolf,")){
                        animal = new Wolf(name);
                    } else if (type.equals("Iguana,")){
                        animal = new Iguana(name);
                    } else if (type.equals("Parrot,")){
                        animal = new Parrot(name);
                    }

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }

        static void printColors(Animal Animal[]){
        String color = null;
        for (Animal animal : Animal){
                if (animal instanceof Bird){
                    color = ((Bird) animal).getFeatherColor();
                } else if (animal instanceof Lizard){
                    color = ((Lizard) animal).getScaleColor();
                } else if (animal instanceof Mammal){
                    color = ((Mammal) animal).getFurColor();
                }
                System.out.println(animal.getName() + " " + color);
            }

        }

        static void saveToBinaryFile(Animal Animal[], String filePath){
            try {
                ObjectOutputStream outS = new ObjectOutputStream(new FileOutputStream(filePath));
                for (Animal animal : Animal) {
                    outS.writeObject(animal);
                }
                outS.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        static void readFromBinaryFile(String filePath){
            Animal[] animalsArray3 = new Animal[10];
            try {
                ObjectInputStream inS = new ObjectInputStream(new FileInputStream(filePath));
                for (int i = 0; i < 10; i++) {
                    animalsArray3[i] = (Animal) inS.readObject();
                }
                inS.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }





